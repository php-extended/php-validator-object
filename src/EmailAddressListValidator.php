<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

use PhpExtended\Email\EmailAddressListParser;
use PhpExtended\Email\EmailAddressListParserInterface;
use PhpExtended\Parser\ParseThrowable;

/**
 * EmailAddressListValidator class file.
 * 
 * This class validates a single attribute as email address list.
 * 
 * @author Anastaszor
 */
class EmailAddressListValidator extends AttributeValidator
{
	
	/**
	 * The parser.
	 * 
	 * @var ?EmailAddressListParserInterface
	 */
	protected static ?EmailAddressListParserInterface $_parser = null;
	
	/**
	 * Gets the parser.
	 * 
	 * @return EmailAddressListParserInterface
	 */
	protected static function getParser() : EmailAddressListParserInterface
	{
		if(null === static::$_parser)
		{
			static::$_parser = new EmailAddressListParser();
		}
		
		return static::$_parser;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsBoolean()
	 */
	public function validateAsBoolean(string $attrName, bool $attrValue) : array
	{
		$message = 'The given attribute {attrName} should be an email address list, not a boolean';
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, null, \strtr($message, $context)),
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsInteger()
	 */
	public function validateAsInteger(string $attrName, int $attrValue) : array
	{
		$message = 'The given attribute {attrName} should be an email address list, not an integer';
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, null, \strtr($message, $context)),
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsFloat()
	 */
	public function validateAsFloat(string $attrName, float $attrValue) : array
	{
		$message = 'The given attribute {attrName} should be an email address list, not a float';
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, null, \strtr($message, $context)),
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsString()
	 */
	public function validateAsString(string $attrName, string $attrValue) : array
	{
		try
		{
			$emailAddressList = $this->getParser()->parse($attrValue);
			
			return [
				new ValidationResult(false, $attrName, $emailAddressList->getCanonicalRepresentation(), 'Parsing Success'),
			];
		}
		catch(ParseThrowable $e)
		{
			return [
				new ValidationResult(true, $attrName, $attrValue, $e->getMessage()),
			];
		}
	}
	
}
