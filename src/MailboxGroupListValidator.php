<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

use PhpExtended\Email\MailboxGroupListParser;
use PhpExtended\Email\MailboxGroupListParserInterface;
use PhpExtended\Parser\ParseThrowable;

/**
 * MailboxGroupListValidator class file.
 * 
 * This class validates a single attribute as mailbox group lists.
 * 
 * @author Anastaszor
 */
class MailboxGroupListValidator extends AttributeValidator
{
	
	/**
	 * The parser.
	 * 
	 * @var ?MailboxGroupListParserInterface
	 */
	protected static ?MailboxGroupListParserInterface $_parser = null;
	
	/**
	 * Gets the parser.
	 * 
	 * @return MailboxGroupListParserInterface
	 */
	protected static function getParser() : MailboxGroupListParserInterface
	{
		if(null === static::$_parser)
		{
			static::$_parser = new MailboxGroupListParser();
		}
		
		return static::$_parser;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsBoolean()
	 */
	public function validateAsBoolean(string $attrName, bool $attrValue) : array
	{
		$message = 'The given attribute {attrName} should be a mailbox group list, not a boolean';
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, null, \strtr($message, $context)),
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsInteger()
	 */
	public function validateAsInteger(string $attrName, int $attrValue) : array
	{
		$message = 'The given attribute {attrName} should be a mailbox group list, not an integer';
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, null, \strtr($message, $context)),
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsFloat()
	 */
	public function validateAsFloat(string $attrName, float $attrValue) : array
	{
		$message = 'The given attribute {attrName} should be a mailbox group list, not a float';
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, null, \strtr($message, $context)),
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsString()
	 */
	public function validateAsString(string $attrName, string $attrValue) : array
	{
		try
		{
			$mailboxGroupList = $this->getParser()->parse($attrValue);
			
			return [
				new ValidationResult(false, $attrName, $mailboxGroupList->getCanonicalRepresentation(), 'Parsing Success'),
			];
		}
		catch(ParseThrowable $e)
		{
			return [
				new ValidationResult(true, $attrName, $attrValue, $e->getMessage()),
			];
		}
	}
	
}
