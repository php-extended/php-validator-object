<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

/**
 * IntegerValidator class file.
 *
 * This represents a validator that validates that attributes are not empty.
 *
 * @author Anastaszor
 */
class RequiredValidator extends AttributeValidator
{
	
	/**
	 * Builds a new RequiredValidator with the given name matchings.
	 * @param array<integer, string> $exactFieldNames
	 * @param array<integer, string> $fieldNamePatterns
	 */
	public function __construct(array $exactFieldNames = [], array $fieldNamePatterns = [])
	{
		parent::__construct($exactFieldNames, $fieldNamePatterns, false);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsNull()
	 */
	public function validateAsNull(string $attrName) : array
	{
		$message = 'The given attribute {attrName} is required and should not be null.';
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, null, \strtr($message, $context)),
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsBoolean()
	 */
	public function validateAsBoolean(string $attrName, bool $attrValue) : array
	{
		if($attrValue)
		{
			return [];
		}
		
		$message = 'The given attribute {attrName} is required and should not be false.';
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, $attrValue, \strtr($message, $context)),
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsInteger()
	 */
	public function validateAsInteger(string $attrName, int $attrValue) : array
	{
		if(0 !== $attrValue)
		{
			return [];
		}
		
		$message = 'The given attribute {attrName} is required and should not be zero.';
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, $attrValue, \strtr($message, $context)),
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsFloat()
	 */
	public function validateAsFloat(string $attrName, float $attrValue) : array
	{
		if(0.0 !== $attrValue)
		{
			return [];
		}
		
		$message = 'The given attribute {attrName} is required and should not be zero.';
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, $attrValue, \strtr($message, $context)),
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsString()
	 */
	public function validateAsString(string $attrName, string $attrValue) : array
	{
		$trimmed = \trim($attrValue);
		if(!empty($trimmed))
		{
			return [];
		}
		
		$message = 'The given attribute {attrName} is required and should not be an empty string.';
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, $attrValue, \strtr($message, $context)),
		];
	}
	
}
