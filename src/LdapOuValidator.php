<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

/**
 * LdapOuValidator class file.
 * 
 * This class validates a string as an organizational unit.
 * 
 * @author Anastaszor
 */
class LdapOuValidator extends LdapStringValidator
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\LdapStringValidator::getBaseRegex()
	 */
	protected function getBaseRegex() : string
	{
		return '[\\w\\d\\s\\-\'/]+';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\LdapStringValidator::getBaseMessage()
	 */
	protected function getBaseMessage() : string
	{
		return 'Only alphanumerical, minus, apostrophe and slashed are allowed';
	}
	
}
