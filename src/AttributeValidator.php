<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

/**
 * AttributeValidator class file.
 * 
 * This class validates a single attribute value.
 * 
 * @author Anastaszor
 */
class AttributeValidator implements AttributeValidatorInterface
{
	
	/**
	 * The exact field names that this attribute validator should match to
	 * validate something.
	 * 
	 * @var array<integer, string>
	 */
	protected array $_exactFieldNames = [];
	
	/**
	 * The patterns of fields that this attribute validator should match to
	 * validate something.
	 * 
	 * @var array<integer, string>
	 */
	protected array $_fieldNamePatterns = [];
	
	/**
	 * Whether this attribute validator allows null values.
	 *
	 * @var boolean
	 */
	protected bool $_allowsNullable = false;
	
	/**
	 * Builds a new AttributeValidator with the given exact name matches, and
	 * the given name patterns to match.
	 * 
	 * @param array<integer, string> $exactFieldNames
	 * @param array<integer, string> $fieldNamePatterns
	 * @param boolean $allowNullable
	 */
	public function __construct(array $exactFieldNames = [], array $fieldNamePatterns = [], bool $allowNullable = false)
	{
		$this->_allowsNullable = $allowNullable;
		$this->_exactFieldNames = $exactFieldNames;
		$this->_fieldNamePatterns = $fieldNamePatterns;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidatorInterface::validate()
	 */
	public function validate(string $attrName, $attrValue) : array
	{
		if(!$this->match($attrName))
		{
			return [];
		}
		
		if(null === $attrValue)
		{
			return $this->validateAsNull($attrName);
		}
		
		if(\is_bool($attrValue))
		{
			return $this->validateAsBoolean($attrName, $attrValue);
		}
		
		if(\is_int($attrValue))
		{
			return $this->validateAsInteger($attrName, $attrValue);
		}
		
		if(\is_float($attrValue))
		{
			return $this->validateAsFloat($attrName, $attrValue);
		}
		
		if(\is_string($attrValue))
		{
			return $this->validateAsString($attrName, $attrValue);
		}
		
		$message = 'The given value with type {type} is not a scalar and cannot be validated.';
		$context = ['{type}' => \gettype($attrValue)];
		
		return [new ValidationResult(true, $attrName, null, \strtr($message, $context))];
	}
	
	/**
	 * Gets whether this attribute validator should care about the given
	 * attribute name.
	 * 
	 * @param string $attrName
	 * @return boolean
	 */
	public function match(string $attrName) : bool
	{
		if(empty($this->_exactFieldNames) && empty($this->_fieldNamePatterns))
		{
			return true;
		}
		
		if(\in_array($attrName, $this->_exactFieldNames, true))
		{
			return true;
		}
		
		foreach($this->_fieldNamePatterns as $pattern)
		{
			/** @psalm-suppress ArgumentTypeCoercion */ // cannot enforce non empty pattern
			if(\preg_match($pattern, $attrName))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Validates this attribute with a null value.
	 * 
	 * @param string $attrName
	 * @return array<integer, ValidationResultInterface>
	 */
	public function validateAsNull(string $attrName) : array
	{
		if($this->_allowsNullable)
		{
			return [];
		}
		
		$message = 'The given attribute {attrName} should not be null.';
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, null, \strtr($message, $context)),
		];
	}
	
	/**
	 * Validates this attribute with a boolean value.
	 * 
	 * @param string $attrName
	 * @param boolean $attrValue
	 * @return array<integer, ValidationResultInterface>
	 * @SuppressWarnings("PHPMD.UnusedFormalParameter")
	 */
	public function validateAsBoolean(string $attrName, bool $attrValue) : array
	{
		return [];
	}
	
	/**
	 * Validates this attribute with an integer value.
	 * 
	 * @param string $attrName
	 * @param integer $attrValue
	 * @return array<integer, ValidationResultInterface>
	 * @SuppressWarnings("PHPMD.UnusedFormalParameter")
	 */
	public function validateAsInteger(string $attrName, int $attrValue) : array
	{
		return [];
	}
	
	/**
	 * Validates this attribute with a float value.
	 * 
	 * @param string $attrName
	 * @param float $attrValue
	 * @return array<integer, ValidationResultInterface>
	 * @SuppressWarnings("PHPMD.UnusedFormalParameter")
	 */
	public function validateAsFloat(string $attrName, float $attrValue) : array
	{
		return [];
	}
	
	/**
	 * Validates this attribute with a string value.
	 * 
	 * @param string $attrName
	 * @param string $attrValue
	 * @return array<integer, ValidationResultInterface>
	 * @SuppressWarnings("PHPMD.UnusedFormalParameter")
	 */
	public function validateAsString(string $attrName, string $attrValue) : array
	{
		return [];
	}
	
}
