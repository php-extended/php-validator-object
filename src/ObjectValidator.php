<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

use RuntimeException;
use Throwable;

/**
 * ObjectValidator class file.
 *
 * This represents a validator that validates objects based on a multiple inner
 * validators.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class ObjectValidator implements ObjectValidatorInterface
{
	
	/**
	 * The attribute validators to apply to the objects.
	 * 
	 * @var array<string, array<int, AttributeValidatorInterface>>
	 */
	protected array $_attributeValidators = [];
	
	/**
	 * The array validators to apply to the objects.
	 * 
	 * @var array<string, array<int, ArrayValidatorInterface>>
	 */
	protected array $_arrayValidators = [];
	
	/**
	 * Builds a new ObjectValidator with the given attribute validators and
	 * array validators.
	 * 
	 * @param array<string, AttributeValidatorInterface> $attributeValidators
	 * @param array<string, ArrayValidatorInterface> $arrayValidators
	 */
	public function __construct(array $attributeValidators = [], array $arrayValidators = [])
	{
		foreach($attributeValidators as $fields => $validator)
		{
			foreach(\explode(',', $fields) as $fieldName)
			{
				$fieldName = \trim($fieldName);
				$this->_attributeValidators[$fieldName][] = $validator;
			}
		}
		
		foreach($arrayValidators as $fields => $validator)
		{
			foreach(\explode(',', $fields) as $fieldName)
			{
				$fieldName = \trim($fieldName);
				$this->_arrayValidators[$fieldName][] = $validator;
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\ObjectValidatorInterface::validate()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function validate($object) : array
	{
		$errors = [];
		
		foreach($this->_attributeValidators as $attrName => $validators)
		{
			try
			{
				$attrValue = $this->getValue($attrName, $object);
			}
			catch(Throwable $thr)
			{
				$message = 'Failed to get value from object of class {class} for attrName {attrName}';
				$context = ['{class}' => \get_class($object), '{attrName}' => $attrName];
				
				$errors[] = new ValidationResult(true, $attrName, null, \strtr($message, $context));
				
				continue;
			}
			
			foreach($validators as $validator)
			{
				/** @var AttributeValidatorInterface $validator */
				$results = $validator->validate($attrName, $attrValue);
				
				foreach($results as $result)
				{
					/** @var ValidationResultInterface $result */
					if(!$result->isError())
					{
						$attrValue = $result->getTransformedValue();
						continue;
					}
					
					$errors[] = $result;
				}
			}
		}
		
		foreach($this->_arrayValidators as $attrName => $validators)
		{
			try
			{
				$attrValues = $this->getValue($attrName, $object);
			}
			catch(Throwable $thr)
			{
				$message = 'Failed to get value from object of class {class} for attrName {attrName}';
				$context = ['{class}' => \get_class($object), '{attrName}' => $attrName];
				
				$errors[] = new ValidationResult(true, $attrName, null, \strtr($message, $context));
				
				continue;
			}
			
			if(!\is_array($attrValues))
			{
				$message = 'The given value from object of class {class} for attrName {attrName} should be an array but is an {type}';
				$context = ['{class}' => \get_class($object), '{attrName}' => $attrName, '{type}' => \gettype($attrValues)];
				
				$errors[] = new ValidationResult(true, $attrName, null, \strtr($message, $context));
				
				continue;
			}
			
			foreach($validators as $validator)
			{
				/** @var ArrayValidatorInterface $validator */
				/** @psalm-suppress MixedArgumentTypeCoercion */
				$results = $validator->validateArray($attrName, (array) $attrValues);
				
				foreach($results as $result)
				{
					/** @var ValidationResultInterface $result */
					if(!$result->isError())
					{
						$attrValues = $result->getTransformedValue();
						continue;
					}
					
					$errors[] = $result;
				}
			}
		}
		
		return $errors;
	}
	
	/**
	 * Tries to get the value that correspond to the given field name.
	 * 
	 * @param string $fieldName
	 * @param object $object
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @todo implements property accessor library like symfony/property-access
	 * @throws Throwable
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @psalm-suppress MixedReturnTypeCoercion
	 */
	public function getValue(string $fieldName, $object)
	{
		if(\property_exists($object, $fieldName))
		{
			try
			{
				$ret = $object->{$fieldName};
				if(null === $ret || \is_scalar($ret) || \is_array($ret) || \is_object($ret))
				{
					/** @psalm-suppress MixedReturnTypeCoercion */
					return $ret;
				}
			}
			/** @phpstan-ignore-next-line */
			catch(Throwable $thr)
			{ // silent
			}
		}
		
		if(\method_exists($object, $fieldName))
		{
			try
			{
				$ret = $object->{$fieldName}();
				if(null === $ret || \is_scalar($ret) || \is_array($ret) || \is_object($ret))
				{
					/** @psalm-suppress MixedReturnTypeCoercion */
					return $ret;
				}
			}
			/** @phpstan-ignore-next-line */
			catch(Throwable $thr)
			{ // silent
			}
		}
		
		$getter = 'get'.\ucfirst($fieldName);
		if(\method_exists($object, $getter))
		{
			try
			{
				$ret = $object->{$getter}();
				if(null === $ret || \is_scalar($ret) || \is_array($ret) || \is_object($ret))
				{
					/** @psalm-suppress MixedReturnTypeCoercion */
					return $ret;
				}
			}
			/** @phpstan-ignore-next-line */
			catch(Throwable $thr)
			{ // silent
			}
		}
		
		$isser = 'is'.\ucfirst($fieldName);
		if(\method_exists($object, $isser))
		{
			try
			{
				$ret = $object->{$isser}();
				if(null === $ret || \is_scalar($ret) || \is_array($ret) || \is_object($ret))
				{
					/** @psalm-suppress MixedReturnTypeCoercion */
					return $ret;
				}
			}
			/** @phpstan-ignore-next-line */
			catch(Throwable $thr)
			{ // silent
			}
		}
		
		$hasser = 'has'.\ucfirst($fieldName);
		if(\method_exists($object, $hasser))
		{
			try
			{
				$ret = $object->{$hasser}();
				if(null === $ret || \is_scalar($ret) || \is_array($ret) || \is_object($ret))
				{
					/** @psalm-suppress MixedReturnTypeCoercion */
					return $ret;
				}
			}
			/** @phpstan-ignore-next-line */
			catch(Throwable $thr)
			{ // silent
			}
		}
		
		if(\method_exists($object, '__get'))
		{
			try
			{
				$ret = $object->__get($fieldName);
				if(null === $ret || \is_scalar($ret) || \is_array($ret) || \is_object($ret))
				{
					/** @psalm-suppress MixedReturnTypeCoercion */
					return $ret;
				}
			}
			/** @phpstan-ignore-next-line */
			catch(Throwable $thr)
			{ // silent
			}
		}
		
		if(\method_exists($object, '__call'))
		{
			try
			{
				$ret = $object->__call($fieldName);
				if(null === $ret || \is_scalar($ret) || \is_array($ret) || \is_object($ret))
				{
					/** @psalm-suppress MixedReturnTypeCoercion */
					return $ret;
				}
			}
			/** @phpstan-ignore-next-line */
			catch(Throwable $thr)
			{ // silent
			}
		}
		
		$message = 'Failed to get {fieldName} data from object of class {class}, tried : {list}';
		$context = [
			'{fieldName}' => $fieldName, '{class}' => \get_class($object), 
			'{list}' => \implode(', ', [$fieldName, $fieldName.'()', $getter.'()', $isser.'()', $hasser.'()', '__get()']),
		];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
}
