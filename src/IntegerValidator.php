<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

/**
 * IntegerValidator class file.
 *
 * This represents a validator that validates attributes with integer values.
 *
 * @author Anastaszor
 */
class IntegerValidator extends AttributeValidator
{
	
	/**
	 * Whether this attribute validator validates only integer values.
	 * 
	 * @var boolean
	 */
	protected bool $_strict = false;
	
	/**
	 * Builds a new IntegerValidator with the given exact name matches and
	 * the given name patterns to match.
	 * 
	 * @param array<integer, string> $exactFieldNames
	 * @param array<integer, string> $fieldNamePatterns
	 * @param boolean $allowNullable
	 * @param boolean $strict
	 */
	public function __construct(array $exactFieldNames = [], array $fieldNamePatterns = [], bool $allowNullable = false, bool $strict = false)
	{
		parent::__construct($exactFieldNames, $fieldNamePatterns, $allowNullable);
		$this->_strict = $strict;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsBoolean()
	 */
	public function validateAsBoolean(string $attrName, bool $attrValue) : array
	{
		if(!$this->_strict)
		{
			return [new ValidationResult(false, $attrName, (int) $attrValue, 'Converted to int.')];
		}
		
		$message = 'Failed to transform {attrName} with boolean value into int (strict mode).';
		$context = ['{attrName}' => $attrName];
		
		return [new ValidationResult(true, $attrName, null, \strtr($message, $context))];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsFloat()
	 */
	public function validateAsFloat(string $attrName, float $attrValue) : array
	{
		if(!$this->_strict)
		{
			return [new ValidationResult(false, $attrName, (int) $attrValue, 'Converted to int.')];
		}
		
		$message = 'Failed to transform {attrName} with float value into int (strict mode).';
		$context = ['{attrName}' => $attrName];
		
		return [new ValidationResult(true, $attrName, null, \strtr($message, $context))];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsString()
	 */
	public function validateAsString(string $attrName, string $attrValue) : array
	{
		if(!$this->_strict && \preg_match('#^\\d+$#', $attrValue))
		{
			return [new ValidationResult(false, $attrName, (int) $attrValue, 'Converted to int.')];
		}
		
		$message = 'Failed to transform {attrName} with string value into int (strict mode).';
		$context = ['{attrName}' => $attrName];
		
		return [new ValidationResult(true, $attrName, null, \strtr($message, $context))];
	}
	
}
