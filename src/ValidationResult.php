<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

/**
 * ValidationResult class file.
 * 
 * This class represents a result from a validation that can be a transform
 * with the new value "canonicalized", or an error with a specific message.
 * 
 * @author Anastaszor
 */
class ValidationResult implements ValidationResultInterface
{
	
	/**
	 * Whether this result is an error.
	 * 
	 * @var boolean
	 */
	protected bool $_isError;
	
	/**
	 * The attribute name.
	 * 
	 * @var string
	 */
	protected string $_attributeName;
	
	/**
	 * The transformed value or the original value.
	 * 
	 * @var null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>
	 */
	protected $_transformedValue;
	
	/**
	 * The error message.
	 * 
	 * @var string
	 */
	protected string $_message;
	
	/**
	 * Builds a new ValidationResult with its inner data.
	 * 
	 * @param boolean $isError
	 * @param string $attrName
	 * @param null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string> $newValue
	 * @param string $message
	 */
	public function __construct(bool $isError, string $attrName, $newValue, string $message)
	{
		$this->_isError = $isError;
		$this->_attributeName = $attrName;
		$this->_transformedValue = $newValue;
		$this->_message = $message;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->_attributeName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\ValidationResultInterface::isError()
	 */
	public function isError() : bool
	{
		return $this->_isError;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\ValidationResultInterface::getAttributeName()
	 */
	public function getAttributeName() : string
	{
		return $this->_attributeName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\ValidationResultInterface::getTransformedValue()
	 */
	public function getTransformedValue()
	{
		return $this->_transformedValue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\ValidationResultInterface::getMessage()
	 */
	public function getMessage() : string
	{
		return $this->_message;
	}
	
}
