<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

use Throwable;

/**
 * InlineValidator class file.
 * 
 * A validator that validates by running the object's given method with
 * given attribute and given params.
 * 
 * @author Anastaszor
 */
class InlineValidator implements ObjectValidatorInterface
{
	
	/**
	 * The method to validate against.
	 *
	 * @var string
	 */
	protected string $_methodName;
	
	/**
	 * The param to validate with.
	 *
	 * @var array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	protected array $_params = [];
	
	/**
	 * Builds a new InlineValidator with the given method and params.
	 *
	 * @param string $methodName
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $params
	 */
	public function __construct(string $methodName, array $params = [])
	{
		$this->_methodName = $methodName;
		$this->_params = $params;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\ObjectValidatorInterface::validate()
	 * @psalm-suppress MixedReturnTypeCoercion
	 */
	public function validate($object) : array
	{
		try
		{
			/** @phpstan-ignore-next-line */
			return (array) \call_user_func_array([$object, $this->_methodName], $this->_params);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $thr)
		{
			$message = 'Failed to trigger inline validation for method {methodName} on object of class {class}';
			$context = ['{methodName}' => $this->_methodName, '{class}' => \get_class($object)];
			
			return [new ValidationResult(true, $this->_methodName, null, \strtr($message, $context))];
		}
	}
	
}
