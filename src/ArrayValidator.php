<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

/**
 * ArrayValidator class file.
 * 
 * This represents a validator that validates array based on a single inner
 * validator.
 * 
 * @author Anastaszor
 */
class ArrayValidator implements ArrayValidatorInterface
{
	
	/**
	 * The inner validator.
	 * 
	 * @var AttributeValidatorInterface
	 */
	protected AttributeValidatorInterface $_innerValidator;
	
	/**
	 * Whether to allow empty arrays.
	 * 
	 * @var boolean
	 */
	protected bool $_allowsEmpty = false;
	
	/**
	 * Builds a new ArrayValidator based on the given inner validator.
	 * 
	 * @param AttributeValidatorInterface $validator
	 * @param boolean $allowsEmpty
	 */
	public function __construct(AttributeValidatorInterface $validator, bool $allowsEmpty = false)
	{
		$this->_innerValidator = $validator;
		$this->_allowsEmpty = $allowsEmpty;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\ArrayValidatorInterface::validateArray()
	 */
	public function validateArray(string $attrName, array $attrValues) : array
	{
		$errors = [];
		$isEmpty = true;
		
		foreach($attrValues as $k => $attrValue)
		{
			$newKey = $attrName.'['.((string) $k).']';
			$errors = \array_merge($errors, $this->_innerValidator->validate($newKey, $attrValue));
			$isEmpty = false;
		}
		
		if($isEmpty && !$this->_allowsEmpty)
		{
			$message = 'The attribute {attrName} is an empty array.';
			$context = ['{attrName}' => $attrName];
			
			$errors[] = new ValidationResult(true, $attrName, [], \strtr($message, $context));
		}
		
		return $errors;
	}
	
}
