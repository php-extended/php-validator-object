<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Validator;

/**
 * LdapStringValidator class file.
 * 
 * This class validates a string, and validates if the string contains only
 * allowed characters. Allowed characters for this class are alphanumerical,
 * spaces, "-" and "'".
 * 
 * This class allows jokers (i.e. character "*") to be used within the search
 * word, but with a policy driven by the constants.
 * 
 * @author Anastaszor
 */
class LdapStringValidator extends AttributeValidator
{
	
	public const ALLOW_JOKER_BEGINNING = 1;
	public const ALLOW_JOKER_MIDDLE = 2;
	public const ALLOW_JOKER_END = 4;
	
	/**
	 * The positions of the jokers to allow.
	 * 
	 * @var integer
	 */
	protected int $_allowJokers = 7;
	
	/**
	 * Builds a new LdapStringValidator with the matching field names and the
	 * jokers to be used.
	 * 
	 * @param array<integer, string> $exactFieldNames
	 * @param array<integer, string> $fieldNamePatterns
	 * @param boolean $allowNullable
	 * @param integer $allowJokers
	 */
	public function __construct(array $exactFieldNames = [], array $fieldNamePatterns = [], bool $allowNullable = false, int $allowJokers = 7)
	{
		parent::__construct($exactFieldNames, $fieldNamePatterns, $allowNullable);
		$this->_allowJokers = $allowJokers;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsBoolean()
	 */
	public function validateAsBoolean(string $attrName, bool $attrValue) : array
	{
		return $this->validateAsString($attrName, (string) $attrValue);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsInteger()
	 */
	public function validateAsInteger(string $attrName, int $attrValue) : array
	{
		return $this->validateAsString($attrName, (string) $attrValue);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsFloat()
	 */
	public function validateAsFloat(string $attrName, float $attrValue) : array
	{
		return $this->validateAsString($attrName, (string) $attrValue);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Validator\AttributeValidator::validateAsString()
	 */
	public function validateAsString(string $attrName, string $attrValue) : array
	{
		$rgx = '^';
		
		if(0 !== ($this->_allowJokers & self::ALLOW_JOKER_BEGINNING))
		{
			$rgx .= '\\*?';
		}
		
		$rgx .= $this->getRegexPrefix().$this->getBaseRegex();
		if(0 !== ($this->_allowJokers & self::ALLOW_JOKER_MIDDLE))
		{
			$rgx .= '(\\*?'.$this->getBaseRegex().')*';
		}
		
		$rgx .= $this->getRegexSuffix();
		
		if(0 !== ($this->_allowJokers & self::ALLOW_JOKER_END))
		{
			$rgx .= '\\*?';
		}
		
		$rgx .= '$';
		
		if(\preg_match('#'.$rgx.'#', $attrValue))
		{
			return [
				new ValidationResult(false, $attrName, $attrValue, 'Transformed to string and regex validation'),
			];
		}
		
		$message = $this->getBaseMessage();
		$context = ['{attrName}' => $attrName];
		
		return [
			new ValidationResult(true, $attrName, $attrValue, \strtr($message, $context)),
		];
	}
	
	/**
	 * Gets the validation regex for a given part between jokers.
	 *
	 * @return string
	 */
	protected function getBaseRegex() : string
	{
		return '[\\w\\d\\s\\-\']+';
	}
	
	/**
	 * Gets a prefix that is applied before all the regex parts.
	 *
	 * @return string
	 */
	protected function getRegexPrefix() : string
	{
		return '';
	}
	
	/**
	 * Gets a suffix that is applied after all the regex parts.
	 *
	 * @return string
	 */
	protected function getRegexSuffix() : string
	{
		return '';
	}
	
	/**
	 * Gets the message that describes the validation regex.
	 *
	 * @return string
	 */
	protected function getBaseMessage() : string
	{
		return 'Attribute {attrName} is not valid : only alphanumerical, minus and apostrophe are allowed';
	}
	
}
