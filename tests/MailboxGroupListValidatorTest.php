<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Validator\MailboxGroupListValidator;
use PHPUnit\Framework\TestCase;

/**
 * MailboxGroupListValidatorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Validator\MailboxGroupListValidator
 *
 * @internal
 *
 * @small
 */
class MailboxGroupListValidatorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MailboxGroupListValidator
	 */
	protected MailboxGroupListValidator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MailboxGroupListValidator();
	}
	
}
