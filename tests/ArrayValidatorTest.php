<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Validator\ArrayValidator;
use PhpExtended\Validator\AttributeValidator;
use PHPUnit\Framework\TestCase;

/**
 * ArrayValidatorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Validator\ArrayValidator
 *
 * @internal
 *
 * @small
 */
class ArrayValidatorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ArrayValidator
	 */
	protected ArrayValidator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ArrayValidator(new AttributeValidator());
	}
	
}
