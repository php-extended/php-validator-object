<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Validator\AcceptLanguageValidator;
use PHPUnit\Framework\TestCase;

/**
 * AcceptLanguageValidatorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Validator\AcceptLanguageValidator
 * @internal
 * @small
 */
class AcceptLanguageValidatorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AcceptLanguageValidator
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testValidateAsBool() : void
	{
		$this->assertTrue($this->_object->validateAsBoolean('attr', true)[0]->isError());
	}
	
	public function testValidateAsInteger() : void
	{
		$this->assertTrue($this->_object->validateAsInteger('attr', 1)[0]->isError());
	}
	
	public function testValidateAsFloat() : void
	{
		$this->assertTrue($this->_object->validateAsFloat('attr', 1.2)[0]->isError());
	}
	
	public function testValidateAsStringSuccess() : void
	{
		$this->assertFalse($this->_object->validateAsString('attr', 'en_US')[0]->isError());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new AcceptLanguageValidator();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
