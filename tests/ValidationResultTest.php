<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-validator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Validator\ValidationResult;
use PHPUnit\Framework\TestCase;

/**
 * ValidationResultTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Validator\ValidationResult
 *
 * @internal
 *
 * @small
 */
class ValidationResultTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ValidationResult
	 */
	protected ValidationResult $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@attrName', $this->_object->__toString());
	}
	
	public function testIsError() : void
	{
		$this->assertTrue($this->_object->isError());
	}
	
	public function testGetAttributeName() : void
	{
		$this->assertEquals('attrName', $this->_object->getAttributeName());
	}
	
	public function testGetTransformedValue() : void
	{
		$this->assertEquals('newVal', $this->_object->getTransformedValue());
	}
	
	public function testGetMessage() : void
	{
		$this->assertEquals('message', $this->_object->getMessage());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ValidationResult(true, 'attrName', 'newVal', 'message');
	}
	
}
